package sandbox

import org.khronos.webgl.Float32Array
import org.khronos.webgl.WebGLRenderingContext
import org.khronos.webgl.WebGLRenderingContext.Companion.ARRAY_BUFFER
import org.khronos.webgl.WebGLRenderingContext.Companion.COLOR_BUFFER_BIT
import org.khronos.webgl.WebGLRenderingContext.Companion.FLOAT
import org.khronos.webgl.WebGLRenderingContext.Companion.STATIC_DRAW
import org.khronos.webgl.WebGLRenderingContext.Companion.TRIANGLE_STRIP
import org.w3c.dom.HTMLCanvasElement
import webgl.GLX
import kotlin.browser.document
import kotlin.browser.window


fun main(args: Array<String>) {
    try {
        Sandbox().run()
    } catch (e: Exception) {
        window.alert(e.message!!)
        throw e
    }
}

class Sandbox{

    val canvas = document.querySelector("#glCanvas") as HTMLCanvasElement
    val gl = canvas.getContext("webgl") as WebGLRenderingContext
    val glx = GLX(gl)

    val program = run {


        val program = glx.initShaderProgram(Glsl.vertexShaderSource, Glsl.fragmentShaderSource)

        val vertexPositionLoc = program.getAttribLocation("aVertexPosition")

        val positionBuffer = gl.createBuffer()
        gl.bindBuffer(ARRAY_BUFFER, positionBuffer)

        val positions = Float32Array(arrayOf(
                0f, 1f,
                1f, 0f,
                -1f, -1f
        ))


        gl.bufferData(
                ARRAY_BUFFER,
                positions,
                STATIC_DRAW
        )

        val numComponents = 2
        val type = FLOAT
        val normalize = false
        val stride = 0
        val offset = 0

        gl.bindBuffer(ARRAY_BUFFER, positionBuffer)

        gl.vertexAttribPointer(
                vertexPositionLoc,
                numComponents,
                type,
                normalize,
                stride,
                offset
        )

        gl.enableVertexAttribArray(
                vertexPositionLoc
        )

        program

    }

    fun draw() {
        gl.clearColor(0f, 0f, 0f, 1f)
        gl.clear(COLOR_BUFFER_BIT)

        gl.useProgram(program.program)

        val vertexOffset = 0
        val vertexCount = 3
        gl.drawArrays(TRIANGLE_STRIP, vertexOffset, vertexCount)
    }

    fun doResize() {
        canvas.width = window.innerWidth
        canvas.height = window.innerHeight
        gl.viewport(0, 0, canvas.width, canvas.height)
        draw()
    }

    fun run() {
        window.onresize = { doResize() }
        doResize()

        window.requestAnimationFrame(::mainLoop)
    }

    val timeStep = 1000.0 / 60
    var lastFrameTs = 0.0
    var delta = 0.0

    fun mainLoop(ts: Double) {
        delta += ts - lastFrameTs
        lastFrameTs = ts

        while (delta >= timeStep) {
            // step forward timeStep
            delta -= timeStep
        }
        draw()

        window.requestAnimationFrame(::mainLoop)
    }

}


