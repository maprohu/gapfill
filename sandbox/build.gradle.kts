import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile




kotlin.sourceSets {
    getByName("main") {
        kotlin.srcDir("build/generated-sources/main/kotlin")
        kotlin.srcDir("src/main/kotlin")
    }
}


dependencies {
    compile(project(":common"))
}


tasks {
    val glsl by registering {

        val glslPath = "$projectDir/src/main/glsl"

        inputs.files(fileTree(glslPath))

        doLast {
            val source = """
package sandbox

object Glsl {

    val vertexShaderSource = ""${'"'}
${File("$glslPath/vertex.glsl").readText()}
        ""${'"'}.trimIndent()

    val fragmentShaderSource = ""${'"'}
${File("$glslPath/fragment.glsl").readText()}
        ""${'"'}.trimIndent()

}
        """.trimIndent()

            val dir = File("$buildDir/generated-sources/main/kotlin")

            dir.mkdirs()

            File(dir, "Glsl.kt").printWriter().use { out ->
                out.print(source)
            }

        }

    }

    "compileKotlin2Js"(Kotlin2JsCompile::class) {
        kotlinOptions {
            outputFile = "$rootDir/../maprohu.bitbucket.io/sandbox/js/sandbox.js"
        }
        dependsOn(glsl)
    }



}
