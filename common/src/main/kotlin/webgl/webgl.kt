package webgl

import org.khronos.webgl.*
import org.khronos.webgl.WebGLRenderingContext.Companion.COMPILE_STATUS
import org.khronos.webgl.WebGLRenderingContext.Companion.FRAGMENT_SHADER
import org.khronos.webgl.WebGLRenderingContext.Companion.LINK_STATUS
import org.khronos.webgl.WebGLRenderingContext.Companion.VERTEX_SHADER



class GLX(val gl: WebGLRenderingContext) {

    enum class ShaderType(val code: Int) {
        Vertex(VERTEX_SHADER),
        Fragment(FRAGMENT_SHADER)
    }

    fun initShaderProgram(vertexShaderSource: String, fragmentShaderSource: String): Program {
        val vertexShader = loadShader(ShaderType.Vertex, vertexShaderSource)
        val fragmentShader = loadShader(ShaderType.Fragment, fragmentShaderSource)

        val shaderProgram = gl.createProgram()
        gl.attachShader(shaderProgram, vertexShader)
        gl.attachShader(shaderProgram, fragmentShader)
        gl.linkProgram(shaderProgram)

        if (!(gl.getProgramParameter(shaderProgram, LINK_STATUS) as Boolean)) {
            throw Exception("Error: ${gl.getProgramInfoLog(shaderProgram)}")
        }

        return Program(shaderProgram!!)
    }

    fun loadShader(type: ShaderType, source: String): WebGLShader {
        val shader = gl.createShader(type.code)
        gl.shaderSource(shader, source)
        gl.compileShader(shader)

        if (!(gl.getShaderParameter(shader, COMPILE_STATUS) as Boolean)) {
            gl.deleteShader(shader)
            throw Exception("Error: ${gl.getShaderInfoLog(shader)}")
        }

        return shader!!
    }


    inner class Program(val program: WebGLProgram) {

        fun getAttribLocation(name: String): Int {
            return gl.getAttribLocation(program, name)
        }

        fun getUniformLocation(name: String): WebGLUniformLocation? {
            return gl.getUniformLocation(program, name)
        }

    }

}