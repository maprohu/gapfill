package mat4

import kotlin.math.PI
import kotlin.math.tan

class Mat4(
        var a11: Float,
        var a12: Float,
        var a13: Float,
        var a14: Float,
        var a21: Float,
        var a22: Float,
        var a23: Float,
        var a24: Float,
        var a31: Float,
        var a32: Float,
        var a33: Float,
        var a34: Float,
        var a41: Float,
        var a42: Float,
        var a43: Float,
        var a44: Float
) {
    constructor() : this(
            1f,0f,0f,0f,
            0f,1f,0f,0f,
            0f,0f,1f,0f,
            0f,0f,0f,1f
    )

    fun mul4x4r(
            b11: Float,
            b12: Float,
            b13: Float,
            b14: Float,
            b21: Float,
            b22: Float,
            b23: Float,
            b24: Float,
            b31: Float,
            b32: Float,
            b33: Float,
            b34: Float,
            b41: Float,
            b42: Float,
            b43: Float,
            b44: Float
    ): Mat4 {
        val a11 = this.a11
        val a12 = this.a12
        val a13 = this.a13
        val a14 = this.a14
        val a21 = this.a21
        val a22 = this.a22
        val a23 = this.a23
        val a24 = this.a24
        val a31 = this.a31
        val a32 = this.a32
        val a33 = this.a33
        val a34 = this.a34
        val a41 = this.a41
        val a42 = this.a42
        val a43 = this.a43
        val a44 = this.a44
        this.a11 = a11 * b11 + a12 * b21 + a13 * b31 + a14 * b41
        this.a12 = a11 * b12 + a12 * b22 + a13 * b32 + a14 * b42
        this.a13 = a11 * b13 + a12 * b23 + a13 * b33 + a14 * b43
        this.a14 = a11 * b14 + a12 * b24 + a13 * b34 + a14 * b44
        this.a21 = a21 * b11 + a22 * b21 + a23 * b31 + a24 * b41
        this.a22 = a21 * b12 + a22 * b22 + a23 * b32 + a24 * b42
        this.a23 = a21 * b13 + a22 * b23 + a23 * b33 + a24 * b43
        this.a24 = a21 * b14 + a22 * b24 + a23 * b34 + a24 * b44
        this.a31 = a31 * b11 + a32 * b21 + a33 * b31 + a34 * b41
        this.a32 = a31 * b12 + a32 * b22 + a33 * b32 + a34 * b42
        this.a33 = a31 * b13 + a32 * b23 + a33 * b33 + a34 * b43
        this.a34 = a31 * b14 + a32 * b24 + a33 * b34 + a34 * b44
        this.a41 = a41 * b11 + a42 * b21 + a43 * b31 + a44 * b41
        this.a42 = a41 * b12 + a42 * b22 + a43 * b32 + a44 * b42
        this.a43 = a41 * b13 + a42 * b23 + a43 * b33 + a44 * b43
        this.a44 = a41 * b14 + a42 * b24 + a43 * b34 + a44 * b44
        return this
    }

    fun perspective(
            fovy: Float,
            aspect: Float,
            znear: Float,
            zfar: Float
    ): Mat4 {
        var f = (1.0 / tan(fovy / 180 * PI / 2)).toFloat()
        var nf = (1.0 / (zfar - znear)).toFloat()
        return this.mul4x4r(
                f / aspect, 0f, 0f, 0f,
                0f, f, 0f, 0f,
                0f, 0f,-(zfar + znear) * nf, -2 * zfar * znear * nf,
                0f, 0f, -1f, 0f
        )
    }

    fun translate(dx: Float, dy: Float, dz: Float): Mat4 {
        return this.mul4x4r(
                1f, 0f, 0f, dx,
                0f, 1f, 0f, dy,
                0f, 0f, 1f, dz,
                0f, 0f, 0f, 1f
        )
    }

    fun toArray(): Array<Float> {
        return arrayOf(
                this.a11, this.a21, this.a31, this.a41,
                this.a12, this.a22, this.a32, this.a42,
                this.a13, this.a23, this.a33, this.a43,
                this.a14, this.a24, this.a34, this.a44
        )
    }
}

