import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

plugins {
    java
    id("kotlin2js") version "1.2.71" apply false
}

allprojects {

    group = "gapfill"
    version = "dev"

    repositories {
        mavenCentral()
    }

}

subprojects {

    apply(plugin = "kotlin2js")

    dependencies {
        compile(kotlin("stdlib-js"))
    }

}

project(":common") {
    tasks {
        "compileKotlin2Js"(Kotlin2JsCompile::class) {
            kotlinOptions {
                outputFile = "$rootDir/../maprohu.bitbucket.io/lib/common/$version/common.js"
            }
        }

    }
}




